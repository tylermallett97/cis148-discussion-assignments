/*
    Name: Tyler Mallett
    Date: 10-17-2018
    This application will demonstrate the creation of a collection of objects.
    There will be input to create individual widget objects that contain a name field and a price.

    Team Member with Branch:

 */

public class Widget {
    // odject fields (object variables, members, etc)
    private String name;
    private double price;

    //Class fields (class variables that are created even without any objects created)
    private final static int MAX_WIDGETS = 5;
    private static int count = 0;

    //Argument-Based Constructor
    public Widget(String name, double price) {

    }

    //No-argument Constructor
    public Widget() {
        this("Generic", 1.0);
    }

    //Object methods
    public String getName() {return name;  }
    public double getPrice() {return price;}
    public void setprice() {}

    //Class methods
    public static int getMaxWidgets() {return MAX_WIDGETS;}
    public static int getcount() {return count;}
    public static void updateCount() { count++;}

}
