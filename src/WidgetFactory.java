/*
    Name: Tyler Mallett
    Date: 10-17-2018
    This application will demonstrate the creation of a collection of objects.
    There will be input to create individual widget objects that contain a name field and a price.

    Team Member with Branch:

 */

public class WidgetFactory {

    public static void main(String[] args) {
        /*
            - Setup a scanner object to read user input
            - Use a loop to
                    - prompt the user to input a widget name and price
                    - store values in a new widget
                    - add the widget to a widget array
                    - update the widget count
            - You can only create up to the maximum of widgets allowed
            - Display the collection of widgets and the average price
         */

    }
}
